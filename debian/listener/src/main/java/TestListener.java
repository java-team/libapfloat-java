/*
 * Expat License
 *
 * Copyright (c) 2024 Pierre Gruet <pgt@debian.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import java.lang.Runtime;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Predicate;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Collectors;
import org.junit.runner.Description;
import org.junit.runner.notification.RunListener;

public class TestListener extends RunListener {

    @Override
    public void testFinished(Description description) throws Exception {
        try (Stream<Path> stream = Files.list(Paths.get(""))) {
            // List all regular files in the current directory.
            List<String> filesInDir = stream.filter(file -> !Files.isDirectory(file))
                                           .map(Path::getFileName)
                                           .map(Path::toString)
                                           .collect(Collectors.toList());

            // Only keep files ending with ".ap" in the list.
            List<String> filteredListOfFiles = filesInDir.stream()
                                               .filter(new Predicate<String>() {
                    @Override
                    public boolean test(String s) {
                        return s.endsWith(".ap");
                    }
                }).collect(Collectors.toList());
            
            // Remove these files.
            for (String p : filteredListOfFiles) {
                File myFile = new File(p);
                if (!myFile.delete()) {
                    System.err.println("Failed to delete file " + p);
                }
            }
        }
    }
}
